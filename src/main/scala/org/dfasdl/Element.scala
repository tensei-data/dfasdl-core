/*
 * Copyright (c) 2014 - 2021 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.dfasdl

import org.dfasdl.types._

/** An enumeration of all element names of the DFASDL.
  */
sealed abstract class Element extends Product with Serializable {

  /** The tag name of the element in the DFASDL XML.
    *
    * @return
    *   A string containing the XML tag name of the element.
    */
  def tagName: XmlTagName

}

object Element {
  val values: List[Element] = List(
    BINARY,
    BINARY_64,
    BINARY_HEX,
    CHOICE,
    CONSTANT,
    CUSTOM_ID,
    DATE,
    DATETIME,
    CHOICE_ELEMENT,
    ELEMENT,
    FIXED_SEQUENCE,
    FORMATTED_NUMBER,
    FORMATTED_STRING,
    FORMATTED_TIME,
    NUMBER,
    REFERENCE,
    ROOT,
    SCALA_EXPRESSION,
    SEQUENCE,
    STRING,
    TIME
  )

  private val mappings: Map[XmlTagName, Element] = values.map(e => (e.tagName -> e)).toMap

  /** Return an element type corresponding to the given tag name.
    *
    * @param tagName
    *   A string containing an XML tag name.
    * @return
    *   An option to the corresponding DFASDL element type.
    */
  def from(tagName: String): Option[Element] =
    // Would be nicer to write this line but it caused problems on 2.11:
    // XmlTagName.from(tagName).toOption.flatMap(n => mappings.get(n))
    XmlTagName.from(tagName) match {
      case Left(_)  => None
      case Right(n) => mappings.get(n)
    }

  case object BINARY extends Element { override val tagName: XmlTagName = "bin" }

  case object BINARY_64 extends Element { override val tagName: XmlTagName = "bin64" }

  case object BINARY_HEX extends Element { override val tagName: XmlTagName = "binHex" }

  case object CHOICE extends Element { override val tagName: XmlTagName = "choice" }

  case object CONSTANT extends Element { override val tagName: XmlTagName = "const" }

  case object CUSTOM_ID extends Element { override val tagName: XmlTagName = "cid" }

  case object DATE extends Element { override val tagName: XmlTagName = "date" }

  case object DATETIME extends Element { override val tagName: XmlTagName = "datetime" }

  case object CHOICE_ELEMENT extends Element { override val tagName: XmlTagName = "celem" }

  case object ELEMENT extends Element { override val tagName: XmlTagName = "elem" }

  case object FIXED_SEQUENCE extends Element { override val tagName: XmlTagName = "fixseq" }

  case object FORMATTED_NUMBER extends Element {
    override val tagName: XmlTagName = "formatnum"
  }

  case object FORMATTED_STRING extends Element {
    override val tagName: XmlTagName = "formatstr"
  }

  case object FORMATTED_TIME extends Element {
    override val tagName: XmlTagName = "formattime"
  }

  case object NUMBER extends Element { override val tagName: XmlTagName = "num" }

  case object REFERENCE extends Element { override val tagName: XmlTagName = "ref" }

  case object ROOT extends Element { override val tagName: XmlTagName = "dfasdl" }

  case object SCALA_EXPRESSION extends Element { override val tagName: XmlTagName = "sxp" }

  case object SEQUENCE extends Element { override val tagName: XmlTagName = "seq" }

  case object STRING extends Element { override val tagName: XmlTagName = "str" }

  case object TIME extends Element { override val tagName: XmlTagName = "time" }

}
