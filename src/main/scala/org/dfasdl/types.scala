/*
 * Copyright (c) 2014 - 2021 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.dfasdl

object types {
  type XmlAttributeName = String
  object XmlAttributeName {
    def from(s: String): Either[String, XmlAttributeName] = Option(s).map(_.trim()) match {
      case None | Some("") => Left("Predicate failed: XmlAttributeName must not be empty!")
      case Some(name)      => Right(name)
    }
  }

  type XmlTagName = String
  object XmlTagName {
    def from(s: String): Either[String, XmlTagName] = Option(s).map(_.trim()) match {
      case None | Some("") => Left("Predicate failed: XmlTagName must not be empty!")
      case Some(name)      => Right(name)
    }
  }
}
