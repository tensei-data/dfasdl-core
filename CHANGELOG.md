# ChangeLog

All notable changes to this project will be documented in this file.
This project **does not use** [Semantic Versioning](http://semver.org/).

Version numbers SHALL consist of a major and a minor release number.

## Conventions when editing this file.

Please follow the listed conventions when editing this file:

* one subsection per version
* reverse chronological order (latest entry on top)
* write all dates in iso notation (`YYYY-MM-DD`)
* each version should group changes according to their impact:
    * `Added` for new features.
    * `Changed` for changes in existing functionality.
    * `Deprecated` for once-stable features removed in upcoming releases.
    * `Removed` for deprecated features removed in this release.
    * `Fixed` for any bug fixes.
    * `Security` to invite users to upgrade in case of vulnerabilities.

## Unreleased

## 2.0.2 (2021-12-04)

### Changed

- update Scala.js to 1.7.1
- update Scala 2.13 to 2.13.7
- update Scala 3 to 3.1.0

## 2.0.1 (2021-10-05)

### Added

- publish artefacts also for Scala.JS

## 2.0.0 (2021-09-24)

### Added

- support for Scala 3.0

### Changed

- update Scala 2.12 to 2.12.15

### Removed

- dropped enumeratum because it blocks Scala-3 release
- dropped refined because it blocks Scala-3 release

## 1.1.1 (2021-09-16)

### Changed

- use more modern version of `refined` on later Scala versions
- update SCM and homepage information in publishing settings
- publish artefacts via sonatype
- update Scala language dependencies

## 1.1.0 (2020-02-27)

### Added

- basic Scala types for the XML elements and attributes of the DFASDL

## 1.0.1 (2020-02-05)

- cross publish for Scala 2.11, 2.12 and 2.13
- adjust compiler settings

## 1.0 (2017-06-28)

- initial public release

