// *****************************************************************************
// Projects
// *****************************************************************************

// Calculate the current year for usage in copyright notices and license headers.
lazy val currentYear: Int = java.time.OffsetDateTime.now().getYear

lazy val root =
  project
    .in(file("."))
    .aggregate(dfasdlCore.js, dfasdlCore.jvm)
    .settings(commonSettings)
    .settings(
      crossScalaVersions := Nil, // Workaround for "Repository for publishing is not specified." error
      publish := {},
      publishLocal := {}
    )

lazy val dfasdlCore =
  crossProject(JSPlatform, JVMPlatform)
    .crossType(CrossType.Pure)
    .in(file("."))
    .enablePlugins(
      AsciidoctorPlugin,
      AutomateHeaderPlugin
    )
    .settings(settings)
    .settings(
      name := "dfasdl-core",
      libraryDependencies ++= Seq(
        "org.scalameta" %%% "munit"            % "0.7.29" % Test,
        "org.scalameta" %%% "munit-scalacheck" % "0.7.29" % Test
      ),
      libraryDependencies ++= compilerPlugins(scalaVersion.value)
    )
    .jsSettings(
      coverageEnabled := false,
      scalaJSUseMainModuleInitializer := false,
      Compile / unmanagedResourceDirectories += baseDirectory.value / "src" / "main" / "resources",
      Test / scalaJSUseMainModuleInitializer := false
    )
    .jvmSettings(
      libraryDependencies += "org.scala-js" %%% "scalajs-stubs" % "1.1.0" % Provided,
      Compile / unmanagedResourceDirectories += baseDirectory.value / "src" / "main" / "resources"
    )

// *****************************************************************************
// Library dependencies
// *****************************************************************************

def compilerPlugins(sv: String) =
  CrossVersion.partialVersion(sv) match {
    case Some((2, _)) =>
      Seq(
        compilerPlugin("com.olegpy"    %% "better-monadic-for" % "0.3.1"),
        compilerPlugin("org.typelevel" %% "kind-projector"     % "0.13.2" cross CrossVersion.full)
      )
    case _ =>
      Seq.empty
  }

// *****************************************************************************
// Settings
// *****************************************************************************

lazy val settings =
  commonSettings ++
  documentationSettings ++
  publishSettings ++
  resourceFilterSettings ++
  scalafmtSettings

def compilerSettings(sv: String) =
  CrossVersion.partialVersion(sv) match {
    case Some((2, 11)) =>
      Seq(
      "-deprecation",
      "-encoding", "UTF-8",
      "-explaintypes",
      "-feature",
      "-language:higherKinds",
      "-target:jvm-1.8",
      "-unchecked",
      "-Xcheckinit",
      "-Xfatal-warnings",
      "-Xfuture",
      "-Xlint:adapted-args",
      "-Xlint:by-name-right-associative",
      "-Xlint:delayedinit-select",
      "-Xlint:doc-detached",
      "-Xlint:inaccessible",
      "-Xlint:infer-any",
      "-Xlint:missing-interpolator",
      "-Xlint:nullary-override",
      "-Xlint:nullary-unit",
      "-Xlint:option-implicit",
      "-Xlint:package-object-classes",
      "-Xlint:poly-implicit-overload",
      "-Xlint:private-shadow",
      "-Xlint:stars-align",
      "-Xlint:type-parameter-shadow",
      "-Xlint:unsound-match",
      "-Ydelambdafy:method",
      "-Yno-adapted-args",
      "-Ypartial-unification",
      "-Ywarn-numeric-widen",
      "-Ywarn-unused-import",
      "-Ywarn-value-discard"
    )
    case Some((2, 12)) =>
      Seq(
      "-deprecation",
      "-encoding", "UTF-8",
      "-explaintypes",
      "-feature",
      "-language:higherKinds",
      "-target:jvm-1.8",
      "-unchecked",
      "-Xcheckinit",
      "-Xfatal-warnings",
      "-Xfuture",
      "-Xlint:adapted-args",
      "-Xlint:by-name-right-associative",
      "-Xlint:constant",
      "-Xlint:delayedinit-select",
      "-Xlint:doc-detached",
      "-Xlint:inaccessible",
      "-Xlint:infer-any",
      "-Xlint:missing-interpolator",
      "-Xlint:nullary-override",
      "-Xlint:nullary-unit",
      "-Xlint:option-implicit",
      "-Xlint:package-object-classes",
      "-Xlint:poly-implicit-overload",
      "-Xlint:private-shadow",
      "-Xlint:stars-align",
      "-Xlint:type-parameter-shadow",
      "-Xlint:unsound-match",
      "-Ydelambdafy:method",
      "-Yno-adapted-args",
      "-Ypartial-unification",
      "-Ywarn-numeric-widen",
      "-Ywarn-unused-import",
      "-Ywarn-value-discard"
    )
    case Some((2, 13)) =>
      Seq(
        "-deprecation",
        "-explaintypes",
        "-feature",
        "-language:higherKinds",
        "-unchecked",
        "-Xcheckinit",
        "-Xfatal-warnings",
        "-Xlint:adapted-args",
        "-Xlint:constant",
        "-Xlint:delayedinit-select",
        "-Xlint:doc-detached",
        "-Xlint:inaccessible",
        "-Xlint:infer-any",
        "-Xlint:missing-interpolator",
        "-Xlint:nullary-unit",
        "-Xlint:option-implicit",
        "-Xlint:package-object-classes",
        "-Xlint:poly-implicit-overload",
        "-Xlint:private-shadow",
        "-Xlint:stars-align",
        "-Xlint:type-parameter-shadow",
        "-Ywarn-dead-code",
        "-Ywarn-extra-implicit",
        "-Ywarn-numeric-widen",
        "-Ywarn-unused:implicits",
        "-Ywarn-unused:imports",
        "-Ywarn-unused:locals",
        "-Ywarn-unused:params",
        "-Ywarn-unused:patvars",
        "-Ywarn-unused:privates",
        "-Ywarn-value-discard",
        "-Ycache-plugin-class-loader:last-modified",
        "-Ycache-macro-class-loader:last-modified",
      )
    case Some((3, _)) =>
      Seq(
        "-deprecation",
        "-encoding", "UTF-8",
        "-explain-types",
        "-feature",
        "-language:higherKinds",
        "-unchecked",
        "-Xfatal-warnings",
        "-Ykind-projector"
      )
    case _ =>
      Seq(
        "-encoding", "UTF-8",
      )
  }

lazy val commonSettings =
  Seq(
    ThisBuild / scalaVersion := "3.1.0",
    crossScalaVersions := Seq(scalaVersion.value, "2.13.7", "2.12.15", "2.11.12"),
    organization := "org.dfasdl",
    organizationName := "Wegtam GmbH",
    startYear := Option(2014),
    licenses += ("MPL-2.0", url("https://www.mozilla.org/en-US/MPL/2.0/")),
    headerLicense := Some(
      HeaderLicense.MPLv2(s"2014 - $currentYear", "Contributors as noted in the AUTHORS.md file")
    ),
    scalacOptions ++= compilerSettings(scalaVersion.value),
    //Compile / compile / wartremoverWarnings ++= Warts.unsafe.filterNot(_ == Wart.Any),
    //Compile / unmanagedSourceDirectories := Seq((Compile / scalaSource).value),
    //Test / unmanagedSourceDirectories := Seq((Test / scalaSource).value),
    Test / parallelExecution := false
)

lazy val documentationSettings =
  Seq(
    Asciidoctor / sourceDirectory := baseDirectory.value / "doc"
  )

lazy val publishSettings =
  Seq(
    credentials += Credentials(Path.userHome / ".sbt" / "sonatype_credentials"),
    developers ++= List(
      Developer(
        "wegtam",
        "Wegtam GmbH",
        "tech@wegtam.com",
        url("https://www.wegtam.com")
      ),
      Developer(
        "jan0sch",
        "Jens Grassel",
        "jens@wegtam.com",
        url("https://www.jan0sch.de")
      )
    ),
    ThisBuild / dynverSeparator := "-",
    ThisBuild / dynverSonatypeSnapshots := true,
    homepage := Option(url("https://codeberg.org/tensei-data/dfasdl-core")),
    pgpSigningKey := Option(sys.env.getOrElse("PUBLISH_SIGNING_KEY", "633EA119CC2D5F249A0F409A002841124A42559E")),
    pomIncludeRepository := (_ => false),
    Test / publishArtifact := false,
    publishMavenStyle := true,
    publishTo := {
      val nexus = "https://oss.sonatype.org/"
      if (isSnapshot.value)
        Some("snapshots".at(nexus + "content/repositories/snapshots"))
      else
        Some("releases".at(nexus + "service/local/staging/deploy/maven2"))
    },
    publish := (publish dependsOn (Test / test)).value,
    scmInfo := Option(ScmInfo(
      url("https://codeberg.org/tensei-data/dfasdl-core"),
      "git@codeberg.org:tensei-data/dfasdl-core.git"
    ))
  )

/*
 * Resource filtering is rather tedious.
 *
 * First we have to exclude the `dfasdl.xsd` from the resources to
 * avoid an error about duplicate dependencies upon packaging.
 * Then we have to parse and filter it according to our needs in the
 * `resourceGenerators` task which is only run upon `package` and `run`.
 *
 * All this using hardcoded filenames. :-(
 */
lazy val resourceFilterSettings =
  Seq(
    unmanagedResources / excludeFilter := "dfasdl.xsd",
    Compile / resourceGenerators += Def.task {
      // Replace the `${version}` token with the current version.
      val s = (Compile / resourceDirectory).value / "org" / "dfasdl" / "dfasdl.xsd"
      val sc = IO.read(s)
      val tc = sc.replaceAll("""\$\{version\}""", version.value)
      val t = (Compile / resourceManaged).value / "org" / "dfasdl" / "dfasdl.xsd"
      IO.write(t, tc)
      Seq(t)
    }.taskValue
  )

lazy val scalafmtSettings =
  Seq(
    scalafmtOnCompile := false,
  )
